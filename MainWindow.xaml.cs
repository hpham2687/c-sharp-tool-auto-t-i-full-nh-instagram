﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using xNet;
namespace Instagram
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    
    public partial class MainWindow : Window
    {
        class viewviews
        {
            public string imgPath { get; set; }
            public string name { get; set; }

        }
        // string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

        List<viewviews> view = new List<viewviews>();
        string PATH = "";
        public MainWindow()
        {

            InitializeComponent();
            saveFolder.Text = @"C:\Users";

        }
        public int i=0;
        public void dowload(string a, int j)
        {
            i++;
            if (PATH == "")
            {
                PATH = saveFolder.Text;
            }

         //   Random rand = new Random();
           
            xNet.HttpRequest http = new xNet.HttpRequest();
            var binImg = http.Get(a).ToMemoryStream().ToArray();
            //string fp="";

            File.WriteAllBytes(""+PATH+"\\"+i+"_"+j+"_.png",binImg);


            view.Add(new viewviews()
            {
                imgPath = @"" + PATH + "\\" + i + "_" + j + "_.png",
                name = "" + i + "_" + j + "_.png"
            });
            status.Text = status.Text + ""+DateTime.Now.ToString()+"    Dowload " + i + "_" + j + "_.png Successfully\n";
           DoEvents();
            //  lsb.Items.Add("3");

        }

        public void lay12anh(string htmls)
        {

            xNet.HttpRequest http3 = new xNet.HttpRequest();
            string u = http3.Get(htmls).ToString();
            var res = Regex.Matches(u, @"""display_url"":""(.*?)""", RegexOptions.Singleline);

            //var k = res[j].Groups[1].ToString();
          
            if (res != null && res.Count > 0)
            {
                for (int j = 0; j < res.Count; j++)
                {
                    //   string kq = res[j].ToString();
                    //   kq = kq.Replace("\\u0026","&");
                   
                        string a = res[j].Groups[1].ToString();
                        //  Process.Start(a);
                        dowload(a, j);

                }
            }

        }
        public string getAfter(xNet.HttpRequest http, ref string beginning, ref string check, ref string name)
        {
            status.Text = "" + DateTime.Now.ToString() + "    Waiting...\n";
            DoEvents();

            string t = "";
            string htmls = "";
            string isNext = "true";
            htmls = http.Get(beginning).ToString();
            var nextCusor = Regex.Matches(htmls.ToString(), @"edge_owner_to_timeline_media"":{""count"":(.*?),""page_info"":{""has_next_page"":(.*?),""end_cursor"":""(.*?)""}", RegexOptions.Singleline);
            string afterCode = "";
            lay12anh(beginning);

            if (nextCusor != null && nextCusor.Count >0)
            {
                if (nextCusor[0].Groups.Count > 2)
                {
                    afterCode = nextCusor[0].Groups[3].ToString();
                }
        }else
        {
                check = "false";
                if (i == 0)
                {
                    status.Text = status.Text + "" + DateTime.Now.ToString() + "   This profile is private! Choose another!\n";
                    DoEvents();
                    MessageBox.Show("unsuccessful");
                    return "";
                }
                else
                {
                    status.Text = status.Text + "" + DateTime.Now.ToString() + "    Done " + i + " images is dowloaded!\n";
                    DoEvents();
                    lsb.ItemsSource = view;
                    status.ScrollToEnd();
                    i = 0;
                    return "";


                }
            }

             isNext = nextCusor[0].Groups[2].ToString();


          
            t = "{\"id\":\""+name+"\",\"first\":12,\"after\":\"" + afterCode+"\"}";
            //Process.Start(beginning);
            string hash = "f045d723b6f7f8cc299d62b57abd500a";
                beginning = "https://www.instagram.com/graphql/query/?query_hash="+hash+"&variables="+ HttpUtility.UrlEncode(t)+ "";

            if (isNext != "true")
            {
                check = "false";
                if (i == 0)
                {
                    status.Text = status.Text + "" + DateTime.Now.ToString() + "   This profile is private! Choose another!\n";
                    DoEvents();
                    MessageBox.Show("unsuccessful");
                    return "";
                }
                else
                {
                    status.Text = status.Text + "" + DateTime.Now.ToString() + "    Done " + i + " images is dowloaded!\n";
                    DoEvents();
                    lsb.ItemsSource = view;
                    status.ScrollToEnd();
                    i = 0;

                    MessageBox.Show("DONE");
                    return "";
                }
            }
            else 
            {
              //  MessageBox.Show("xong 1 lan");
                return getAfter(http, ref beginning, ref check, ref name);
            }

        }
        public static void DoEvents()
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                                                  new Action(delegate { }));
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            xNet.HttpRequest http = new xNet.HttpRequest();
           http.UserAgent = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.144 Mobile Safari/537.36";

            string beginning = "";
            string check = "true";
            string name = "";
            //  string user = "";
                status.Text = status.Text + "" + DateTime.Now.ToString() + "    Getting ID... \n";
            DoEvents();
         

            var dataUrs = http.Get("https://www.instagram.com/" + dit.Text + "/?__a=1");
            var usr = Regex.Matches(dataUrs.ToString(), @"profilePage_([0-9]*)""", RegexOptions.Singleline);
            name= usr[0].Groups[1].ToString();
            status.Text = status.Text+ "" + DateTime.Now.ToString() + "    ID=" + name+ "\n";
            DoEvents();

            beginning = "https://www.instagram.com/"+dit.Text+"/?__a=1";
            //MessageBox.Show(dit.Text);
              getAfter(http,ref beginning,ref check, ref name);
               
            

          


            


        }

        void openFileDialogFileOk(object sender, System.ComponentModel.CancelEventArgs e)

        {

            string fullPathname = (sender as OpenFileDialog).FileName;

            FileInfo src = new FileInfo(fullPathname);

           // UploadFile(src.ToString());


        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            {
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();                
            }
             PATH = dialog.SelectedPath.ToString();
            saveFolder.Text = "";
            saveFolder.Text = PATH;
            DoEvents();

            // MessageBox.Show(PATH);
        }

    
    }
}
